# ELI Router

This service redirects a [French ELI URL](http://www.eli.fr) to [Légifrance](https://www.legifrance.gouv.fr). French ELI URLs can represent most of the French laws or law parts (articles, sections, etc.) in their initial or consolidated version or at some date.

Possibly in the future it will display metadata about the requested text or redirect to another law repository, by specifying some HTTP headers like Accept.

This service needs the [legi.py](https://github.com/Legilibre/legi.py) database.

## Run this service

### Manually

1. Clone this project:
   ```sh
   git clone https://framagit.org/parlement-ouvert/eli-router
   ```
2. Install npm dependencies:
   ```sh
   npm install --production
   ```
3. Maintain a legi.sqlite database up-to-date: every day, you have to:
   * download the official LEGI database on [echanges.dila.gouv.fr](https://echanges.dila.gouv.fr/OPENDATA/LEGI/),
   * run the script `tar2sqlite.py` of legi.py.
4. Run the server:
   ```sh
   ./index.js /path/to/legi.sqlite [--port=8080] [--no-index] [--logging] [--debug]
   ```

### With Docker

1. Build the Docker container:
   a. Either the production image:
      ```sh
      docker build -t eli-router https://framagit.org/parlement-ouvert/eli-router
      ```
   a. Either the local image during development:
      ```sh
      docker build -t eli-router .
      ```
2. Maintain a legi.sqlite database up-to-date: every day, you have to:
2. Maintain a legi.sqlite database up-to-date: every day, you have to:
   * download the official LEGI database on [echanges.dila.gouv.fr](https://echanges.dila.gouv.fr/OPENDATA/LEGI/),
   * run the script `tar2sqlite.py` of legi.py.
3. Run the Docker container:
   ```sh
   docker run --rm -p EXPOSED_PORT:80 -v /path/to/your/legi.sqlite/folder:/mnt/legi:ro eli-router [--no-index] [--logging] [--debug]
   ```

### Developers

You can run the server with the following command to restart the server every time you changed the files:
```sh
npm run-script develop -- /path/to/legi.sqlite [--port=8080] [--no-index] [--logging] [--debug]
```

## Useful Links

* [ELI - URI common building blocks for France](http://www.eli.fr/en/constructionURI.html)
* [Technical specs at the European level](http://publications.europa.eu/mdr/eli/)
