FROM alpine:3.8
VOLUME ["/mnt/legi"]
EXPOSE 80
WORKDIR /opt/eli-router

COPY . .

RUN apk add --update --no-cache nodejs npm && npm install --production && apk del --purge npm

ENTRYPOINT ["node", "/opt/eli-router/index.js", "--port=80", "/mnt/legi/legi.sqlite"]
